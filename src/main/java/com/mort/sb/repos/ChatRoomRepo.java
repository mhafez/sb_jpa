package com.mort.sb.repos;

import com.mort.sb.entities.ChatRoom;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface ChatRoomRepo extends ReactiveMongoRepository<ChatRoom, String> {
    @Tailable
    Mono<ChatRoom> findChatRoomByChatRoomId(String chatRoomId);
}
