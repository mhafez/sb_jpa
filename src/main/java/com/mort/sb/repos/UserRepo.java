package com.mort.sb.repos;

import com.mort.sb.entities.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface UserRepo extends ReactiveMongoRepository<User, String>
{
    Flux<User> findUserByUserId(String userId);
}
