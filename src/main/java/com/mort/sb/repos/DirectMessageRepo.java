package com.mort.sb.repos;

import com.mort.sb.entities.DirectMessage;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface DirectMessageRepo extends ReactiveMongoRepository<DirectMessage, String> {
    @Tailable
    Flux<DirectMessage> findByFromUserId(String fromUserId);
}
