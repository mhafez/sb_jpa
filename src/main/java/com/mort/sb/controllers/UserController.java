package com.mort.sb.controllers;

import com.mort.sb.entities.DirectMessage;
import com.mort.sb.entities.User;
import com.mort.sb.repos.DirectMessageRepo;
import com.mort.sb.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserRepo userRepo;
    @Autowired
    DirectMessageRepo directMessageRepo;

    private ExecutorService executor
            = Executors.newCachedThreadPool();

    @GetMapping(path = "/")
    public Flux<User> getAllUsers() {
        return userRepo.findAll();
    }

    @PostMapping(path = "/")
    public Mono<User> addUser(@RequestBody User user) {
        return userRepo.save(user);
    }

    @PostMapping(path = "/dm/{toUser}/")
    public Mono<DirectMessage> addDirectMessage(@PathVariable("toUser") String toUserId, @RequestBody DirectMessage directMessage) {
        directMessage.setDateSent(new Date());
        directMessage.setToUserId(toUserId);
        return directMessageRepo.save(directMessage);
    }

    @GetMapping(path = "/dm/{id}/", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<DirectMessage> getChatMessages(@PathVariable("id") String id) {
        return directMessageRepo.findByFromUserId(id);
    }

    @GetMapping(path = "/{id}/", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<User> getUser(@PathVariable("id") String id) {
        return userRepo.findUserByUserId(id);
    }
}
