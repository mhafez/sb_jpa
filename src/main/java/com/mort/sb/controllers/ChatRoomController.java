package com.mort.sb.controllers;

import com.mort.sb.entities.ChatMessage;
import com.mort.sb.entities.ChatRoom;
import com.mort.sb.repos.ChatMessageRepo;
import com.mort.sb.repos.ChatRoomRepo;
import com.mort.sb.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;

@RestController
@RequestMapping(path = "/chatRoom")
public class ChatRoomController {
    @Autowired
    ChatRoomRepo chatRoomRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    ChatMessageRepo chatMessageRepo;

    @GetMapping(path = "/")
    public Flux<ChatRoom> getAll() {
        return chatRoomRepo.findAll();
    }

    @PostMapping(path = "/")
    public Mono<ChatRoom> addChatRoom(@RequestBody ChatRoom chatRoom) {
        return chatRoomRepo.save(chatRoom);
    }

    @PostMapping(path = "/{id}/")
    public void addChatMessage(@PathVariable("id") String id, @RequestBody ChatMessage chatMessage) {
        chatMessage.setDateSent(new Date());
        chatMessage.setChatRoomId(id);
        chatMessageRepo.save(chatMessage).block();
    }

    @GetMapping(path = "/{id}/", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ChatMessage> getChatMessages(@PathVariable("id") String id) {
        return chatMessageRepo.findChatMessageByChatRoomId(id);
    }

}
