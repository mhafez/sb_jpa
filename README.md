### Programming challenge ###
Design and implement a chat server (and supporting api’s) which can handle multiple users at the same time.

Chat users should be able to do all of the following:

* [ ] sign in and sign out
  
* [x] list existing chat rooms and create new chat rooms  

* [x] post messages to any chat room (there is no need to join the room)  

* [x] post messages to any other user  

* [x] list messages from any chat room  

* [x] list messages posted to directly them  

* [x] subscribe for real time delivery of messages from any chat room (http://localhost:8080/chatRoom/5e77f90bcb65031ec90e7c35/)  

* [x] subscribe for real time delivery of messages posted to directly them (http://localhost:8080/users/dm/5e77f916cb65031ec90e7c36/)   



# Need to run:
### If creating new:
```
db.createCollection("user", {capped: true, size: 5000000, max: 10000})
db.createCollection("chatRoom", {capped: true, size: 5000000, max: 10000})
db.createCollection("directMessage", {capped: true, size: 5000000, max: 10000})
db.createCollection("chatMessage", {capped: true, size: 5000000, max: 10000})
```
### If already running and not sure if collections are capped:
```
db.user.stats()
db.chatRoom.stats()
db.directMessage.stats()
db.chatMessage.stats()
```
### To convert collections to be capped:
```
db.runCommand({"convertToCapped": "user", size: 5000000});
db.runCommand({"convertToCapped": "chatRoom", size: 5000000});
db.runCommand({"convertToCapped": "directMessage", size: 5000000});
db.runCommand({"convertToCapped": "chatMessage", size: 5000000});
```
