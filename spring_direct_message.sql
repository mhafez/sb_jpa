INSERT INTO spring.direct_message (message_id, date_sent, message, from_user_id, to_user_id)
VALUES (1, null, 'sending a message to user 2 from user 1', 1, 2);
INSERT INTO spring.direct_message (message_id, date_sent, message, from_user_id, to_user_id)
VALUES (2, null, 'sending a message to user 2 from user 1', 1, 2);
INSERT INTO spring.direct_message (message_id, date_sent, message, from_user_id, to_user_id)
VALUES (3, '2020-03-21 21:14:57.915000000', 'sending a message to user 2 from user 1', 1, 2);
INSERT INTO spring.direct_message (message_id, date_sent, message, from_user_id, to_user_id)
VALUES (4, '2020-03-21 21:15:10.408000000', 'sending a message to user 2 from user 1', 1, 2);
INSERT INTO spring.direct_message (message_id, date_sent, message, from_user_id, to_user_id)
VALUES (5, '2020-03-21 21:20:09.787000000', 'sending a message to user 2 from user 1', 1, 2);
INSERT INTO spring.direct_message (message_id, date_sent, message, from_user_id, to_user_id)
VALUES (6, '2020-03-21 21:21:08.168000000', 'sending a message to user 2 from user 1', 1, 2);
INSERT INTO spring.direct_message (message_id, date_sent, message, from_user_id, to_user_id)
VALUES (7, '2020-03-21 21:21:24.126000000', 'sending a message to user 2 from user 1', 1, 2);
INSERT INTO spring.direct_message (message_id, date_sent, message, from_user_id, to_user_id)
VALUES (8, '2020-03-21 21:25:06.064000000', 'sending a message to user 2 from user 1', 1, 2);
INSERT INTO spring.direct_message (message_id, date_sent, message, from_user_id, to_user_id)
VALUES (9, '2020-03-21 21:25:40.191000000', 'sending a message to user 2 from user 1', 2, 2);
