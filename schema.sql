create schema spring collate utf8mb4_0900_ai_ci;

create table chat_room
(
    chat_room_id bigint auto_increment
        primary key,
    description  varchar(255) null,
    name         varchar(255) null
);

create table chat_room_users
(
    chat_room_id bigint not null,
    user_id      bigint not null,
    primary key (chat_room_id, user_id)
);

create table user
(
    user_id bigint auto_increment
        primary key,
    name    varchar(255) null
);

create table chat_message
(
    message_id   bigint auto_increment
        primary key,
    date_sent    datetime(6)  null,
    message      varchar(255) null,
    chat_room_id bigint       not null,
    user_id      bigint       not null,
    constraint FKf7tbywofv1iojpxc1kw8c3bx7
        foreign key (user_id) references user (user_id),
    constraint FKj52yap2xrm9u0721dct0tjor9
        foreign key (chat_room_id) references chat_room (chat_room_id)
);

create table direct_message
(
    message_id   bigint auto_increment
        primary key,
    date_sent    datetime(6)  null,
    message      varchar(255) null,
    from_user_id bigint       not null,
    to_user_id   bigint       not null,
    constraint FK2km0p992jxi45nexb5ax42kqx
        foreign key (to_user_id) references user (user_id),
    constraint FKgw658rl3yqn4di1puv3efsb6q
        foreign key (from_user_id) references user (user_id)
);

